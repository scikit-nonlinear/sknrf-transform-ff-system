import os
from enum import Enum
from scipy import interpolate

import math as mt
import torch as th

from sknrf.enums.runtime import SI, si_dtype_map
from sknrf.enums.device import Instrument
from sknrf.enums.signal import transform_color_map
from sknrf.settings import Settings
from sknrf.device.signal import ff
from sknrf.model.transform.ff.base import FFTransform
from sknrf.app.dataviewer.model.snp import SNP
from sknrf.utilities.rf import z2zp, zp2z, s2a, a2s
from sknrf.utilities.rf import viz2baz, baz2viz, a2v, v2a, a2t, t2tp
from sknrf.utilities.rf import n2t
from sknrf.utilities.numeric import Info, PkAvg, Format, Domain, bounded_property


class LineCode(Enum):
    NRZ = 0
    RZ = 1
    DOUBLET = 2


class CascadeTransform(FFTransform):
    r"""Cascaded Circuit Transform

    ..  figure:: ../../_images/PNG/cascade_circuit_transform.png
        :width: 50 %
        :align: center

    .. math::
        \left[\begin{array}{c} b_p \\ a_p \end{array} \right]_{jl} &=&
        \left[\begin{array}{cc} T_{p11} & T_{p12} \\
                                T_{p21} & T_{p22} \\ \end{array} \right]_{jl}^{-1}
        \left[\begin{array}{c} b_p' \\ a_p' \end{array} \right]_{jl}\\
        \left[ \Gamma \right]_{jl} &=&
        \left[ \frac{T_{p12} - T_{p22} \Gamma'}{T_{p21} \Gamma' - T_{p11}} \right]_{jl}

    A cascaded circuit transform on port j, frequency l.

    Parameters
    ----------
    name : str
        Name of the transform instance.
    ports : list
        List of ports where the transform is applied.
    filename : str
        Filename of (.s2p) S-parameter file that contains the transform coefficients.

    """
    _num_ports = 1
    _domain = Domain.FF
    _device = "cpu"
    _preview_filename = ":/PNG/unknown_circuit_transform.png"
    _default_filename = "Thru.s2p"
    _color_code = Settings().color_map[transform_color_map[Domain.FF]]
    display_order = ["name", "ports", "instrument_flags", "file", "transpose", "swap_ports"]
    optimize = True

    def __new__(cls, name: str = "Cascade", ports: tuple = (1,), instrument_flags=Instrument.RF,
                abcd: th.Tensor = th.eye(2, dtype=si_dtype_map[SI.G]),
                filename: str = "", transpose: bool = False, swap_ports: bool = False):
        self = super(CascadeTransform, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                                    data=abcd)
        default_filename = os.sep.join([Settings().root, 'data', 'saved_transforms', self._default_filename])
        self._filename = default_filename if not filename else filename
        self._file = open(self._filename, mode='r')
        self._file.close()
        self._transpose = transpose
        self._swap_ports = swap_ports
        return self

    def __getnewargs__(self):
        return tuple(list(super(CascadeTransform, self).__getnewargs__()) +
                     [self._data_, self._filename, self._transpose, self._swap_ports])

    def __init__(self, name: str = "Cascade", ports: tuple = (1,), instrument_flags=Instrument.RF,
                 abcd: th.Tensor = th.eye(2, dtype=si_dtype_map[SI.G]),
                 filename: str = "", transpose: bool = False, swap_ports: bool = False):
        super(CascadeTransform, self).__init__(name, ports, instrument_flags=instrument_flags,
                                               data=abcd)
        self.__info__()
        default_filename = os.sep.join([Settings().root, 'data', 'saved_transforms', self._default_filename])
        self._filename = default_filename if not filename else filename
        self._file = open(self._filename, mode='r')
        self._file.close()

    def __info__(self):
        super(CascadeTransform, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["transpose"] = Info("transpose", read=True, write=True, check=False)
        self.info["swap_ports"] = Info("swap ports", read=True, write=True, check=False)

    @property
    def transpose(self):
        return self._transpose

    @transpose.setter
    def transpose(self, transpose):
        self._transpose = transpose
        self._set_data(self._abcd)

    @property
    def swap_ports(self):
        return self._swap_ports

    @swap_ports.setter
    def swap_ports(self, swap_ports):
        self._swap_ports = swap_ports
        self._set_data(self._abcd)

    @property
    def instrument_flags(self):
        return self._instrument_flags

    @instrument_flags.setter
    def instrument_flags(self, instrument_flags):
        self._instrument_flags = instrument_flags
        self._set_data(self._abcd)

    @property
    def file(self):
        return self._file

    @file.setter
    def file(self, file):
        with open(file.name, mode='r') as f:
            freq = ff.freq()
            snp = SNP.read(f.name)
            snp_freq = snp.dataBlocks[0].data[0].independent.real

            snp_dep = snp.dataBlocks[0].data[0].dependents
            s = th.zeros((freq.shape[0], snp_dep.shape[1]), dtype=si_dtype_map[SI.B])
            freq[freq < snp_freq[-0]] = snp_freq[0]
            freq[freq > snp_freq[-1]] = snp_freq[-1]
            for s_index in range(0, snp_dep.shape[1]):
                freq_interp = interpolate.interp1d(snp_freq, snp_dep[:, s_index], kind="linear", assume_sorted=True)
                s[:, s_index] = n2t(freq_interp(freq))
            s = s.reshape(Settings().f_points, Settings().t_points, 2, 2)
            s = s.permute(-3, -4, -2, -1)
            s = s.reshape(-1, 2, 2)
            a = s2a(s)
            a = th.inverse(a)
            self._set_data(a)
            self._filename = f.name
            self._file = f

    def _format_data(self, data):
        t_points, f_points = Settings().t_points, Settings().f_points
        if not self.instrument_flags & Instrument.LF:
            freq_slice = slice(t_points*0, t_points*1, 1)
            data[freq_slice, 0, 0] = data[freq_slice, 1, 1] = 1.0
            data[freq_slice, 1, 0] = data[freq_slice, 0, 1] = 0.0
        if not self.instrument_flags & Instrument.RF:
            freq_slice = slice(t_points*1, t_points*f_points, 1)
            data[freq_slice, 0, 0] = data[freq_slice, 1, 1] = 1.0
            data[freq_slice, 1, 0] = data[freq_slice, 0, 1] = 0.0
        if self.transpose or self.swap_ports:
            data = a2s(data)
            data = data.permute(-3, -1, -2)
            if self.swap_ports:
                temp = data[:, 0, 0]
                data[..., 1, 1] = data[..., 0, 0]
                data[..., 1, 1] = temp
            data = s2a(data)
        return data

    def _set_data(self, data):
        data = data.repeat(Settings().f_points * Settings().t_points, 1, 1) if data.dim() == 2 else data
        data = self._format_data(data)
        super(CascadeTransform, self)._set_data(data)

    def get_stimulus(self, *args):
        p = self.ports[0]
        v, i, z = args[p][0], args[p][1], args[p][2]
        s = v.shape

        z_p = z
        if self.instrument_flags & Instrument.ZTUNER:
            shape = (-1, Settings().t_points*Settings().f_points)
            z = z.reshape(shape)
            z_p = z2zp(z, self._abcd_inv)

        if self.instrument_flags & Instrument.STIMULUS:
            tp = t2tp(a2t(self._abcd)[0], z.flatten(), z_p.flatten())[0]
            gain = tp[:, 1::2, 1::2]
            shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
            v, z, z_p = v.reshape(shape), z.reshape(shape), z_p.reshape(shape)
            v = a2v(th.matmul(gain, v2a(v, z)[0]), z_p)[0]

        res = list(args)
        res[p][0], res[p][1], res[p][2] = v.reshape(s), i.reshape(s), z_p.reshape(s)
        return tuple(res)

    def set_stimulus(self, *args):
        p = self.ports[0]
        v, i, z_p = args[p][0], args[p][1], args[p][2]
        s = v.shape

        z = z_p
        if self.instrument_flags & Instrument.ZTUNER:
            shape = (-1, Settings().t_points*Settings().f_points)
            z_p = z_p.reshape(shape)
            z = zp2z(z_p, self._abcd_inv)

        if self.instrument_flags & Instrument.STIMULUS:
            tp = t2tp(a2t(self._abcd)[0], z.flatten(), z_p.flatten())[0]
            gain_inv = 1/tp[:, 1::2, 1::2]
            shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
            v, z, z_p = v.reshape(shape), z.reshape(shape), z_p.reshape(shape)
            v = a2v(th.matmul(gain_inv, v2a(v, z_p)[0]), z)[0]

        res = list(args)
        res[p][0], res[p][1], res[p][2] = v.reshape(s), i.reshape(s), z.reshape(s)
        return tuple(res)

    def get_response(self, *args):
        p = self.ports[0]
        v, i, z = args[p][0], args[p][1], args[p][2]
        s = v.shape

        z_p = z
        if self.instrument_flags & Instrument.ZTUNER:
            shape = (-1, Settings().t_points*Settings().f_points)
            z = z.reshape(shape)
            z_p = z2zp(z, self._abcd_inv)

        if self.instrument_flags & Instrument.RESPONSE:
            shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
            v, i = v.reshape(shape), i.reshape(shape)
            vi = th.cat((v, i), dim=-2)
            vi = th.matmul(self._abcd, vi)
            v, i = th.split(vi, 1, dim=-2)

        res = list(args)
        res[p][0], res[p][1], res[p][2] = v.reshape(s), i.reshape(s), z_p.reshape(s)
        return tuple(res)


class SourceIQGainTransform(FFTransform):
    r"""Source IQ Gain Transform

    ..  figure:: ../../_images/PNG/cascade_circuit_transform.png
        :width: 50 %
        :align: center

    Same as the Cascade Transform except it applies the gain to the incident a_p wave.

    Parameters
    ----------
    name : str
        Name of the transform instance.
    ports : list
        List of ports where the transform is applied.
    gain : float
        Input IQ Gain

    """

    _num_ports = 1
    _domain = Domain.FF
    _device = "cpu"
    _preview_filename = ":/PNG/unknown_circuit_transform.png"
    _default_filename = "Thru.s2p"
    _color_code = Settings().color_map[transform_color_map[Domain.FF]]
    display_order = ["name", "ports", "instrument_flags", "gain"]
    optimize = False

    def __new__(cls, name: str = "Source IQ Gain", ports: tuple = (1,), instrument_flags=Instrument.RFSOURCE,
                gain: th.Tensor = th.tensor([1.0])):
        self = super(SourceIQGainTransform, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                                         data=th.as_tensor(gain))
        self._gain = gain
        return self

    def __getnewargs__(self):
        return tuple(list(super(SourceIQGainTransform, self).__getnewargs__()) +
                     [self._gain])

    def __init__(self, name: str = "Source IQ Gain", ports: tuple = (1,), instrument_flags=Instrument.RFSOURCE,
                 gain: th.Tensor = th.tensor([1.0])):
        super(SourceIQGainTransform, self).__init__(name, ports, instrument_flags=instrument_flags,
                                                    data=th.as_tensor(gain))
        self.__info__()
        self._set_data(self._gain)

    def __setstate__(self, state):
        self.__info__()
        self._set_data(self._gain)

    def __info__(self):
        super(SourceIQGainTransform, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["file"] = Info("file", read=False, write=False, check=False)
        self.info["gain"] = Info("gain", read=True, write=True, check=False, min_=0.5, max_=2)

    @bounded_property
    def gain(self):
        return self._gain

    @gain.setter
    def gain(self, gain):
        self._gain = gain
        self._set_data(self._gain)

    @property
    def instrument_flags(self):
        return self._instrument_flags

    @instrument_flags.setter
    def instrument_flags(self, instrument_flags):
        self._instrument_flags = instrument_flags
        self._set_data(self._gain)

    def _format_data(self, data):
        t_points, f_points = Settings().t_points, Settings().f_points
        data = data + th.zeros((t_points*f_points, 1, 1), dtype=data.dtype)
        if not self.instrument_flags & Instrument.LF:
            freq_slice = slice(t_points*0, t_points*1, 1)
            data[freq_slice, 0, 0] = 1.0
        if not self.instrument_flags & Instrument.RF:
            freq_slice = slice(t_points*1, t_points*f_points, 1)
            data[freq_slice, 0, 0] = 1.0
        data = data.reshape(f_points, t_points, 1, 1)
        data = data.permute(-3, -4, -2, -1)
        data = data.reshape(-1, 1, 1)
        return data

    def _set_data(self, data):
        data = data.repeat(Settings().f_points * Settings().t_points, 1, 1) if data.dim() == 2 else data
        data = self._format_data(data)
        self._data_ = data

    def get_stimulus(self, *args):
        p = self.ports[0]
        v = args[p][0]
        s = v.shape

        shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
        v = v.reshape(shape)
        a = v2a(v)[0]
        a *= self._data_
        v = a2v(a)[0]

        res = list(args)
        res[p][0] = v.reshape(s)
        return tuple(res)

    def set_stimulus(self, *args):
        p = self.ports[0]
        v = args[p][0]
        s = v.shape

        shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
        v = v.reshape(shape)
        a = v2a(v)[0]
        a /= self._data_
        v = a2v(a)[0]

        res = list(args)
        res[p][0] = v.reshape(s)
        return tuple(res)

    def get_response(self, *args):
        return args


class SourceGainTransform(CascadeTransform):
    r"""Input Block Transform

    ..  figure:: ../../_images/PNG/cascade_circuit_transform.png
        :width: 50 %
        :align: center

    Same as the Cascade Transform except it only applies the S21 to the incident a_p wave.

    Parameters
    ----------
    name : str
        Name of the transform instance.
    ports : list
        List of ports where the transform is applied.
    filename : str
        Filename of (.s2p) S-parameter file that contains the transform coefficients.

    """

    _num_ports = 1
    _domain = Domain.FF
    _device = "cpu"
    _preview_filename = ":/PNG/cascade_circuit_transform.png"
    _default_filename = "Thru.s2p"
    _color_code = Settings().color_map[transform_color_map[Domain.FF]]
    display_order = ["name", "ports", "instrument_flags", "file", "transpose", "swap_ports"]
    optimize = False

    def __new__(cls, name: str = "Source Gain", ports: tuple = (1,), instrument_flags=Instrument.RFSOURCE,
                abcd: th.Tensor = th.eye(2), filename: str = "", transpose: bool = False, swap_ports: bool = False):
        self = super(SourceGainTransform, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                                       abcd=abcd, filename=filename, transpose=transpose, swap_ports=swap_ports)
        return self

    def __init__(self, name: str = "Source Gain", ports: tuple = (1,), instrument_flags=Instrument.RFSOURCE,
                 abcd: th.Tensor = th.eye(2), filename: str = "", transpose: bool = False, swap_ports: bool = False):
        super(SourceGainTransform, self).__init__(name, ports, instrument_flags=instrument_flags,
                                                  abcd=abcd, filename=filename, transpose=transpose, swap_ports=swap_ports)
        self.__info__()

    def __info__(self):
        super(SourceGainTransform, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###

    def _set_data(self, data):
        data = data.repeat(Settings().f_points * Settings().t_points, 1, 1) if data.dim() == 2 else data
        data = self._format_data(data)
        self._data_ = a2s(data)[:, 1:2, 0:1]

    def get_stimulus(self, *args):
        p = self.ports[0]
        v = args[p][0]
        s = v.shape

        shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
        v = v.reshape(shape)
        a = v2a(v)[0]
        a *= self._data_
        v = a2v(a)[0]

        res = list(args)
        res[p][0] = v.reshape(s)
        return tuple(res)

    def set_stimulus(self, *args):
        p = self.ports[0]
        v = args[p][0]
        s = v.shape

        shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
        v = v.reshape(shape)
        a = v2a(v)[0]
        a /= self._data_
        v = a2v(a)[0]

        res = list(args)
        res[p][0] = v.reshape(s)
        return tuple(res)

    def get_response(self, *args):
        return args


class ReceiverGainTransform(CascadeTransform):
    r"""Output Block Transform

    ..  figure:: ../../_images/PNG/cascade_circuit_transform.png
        :width: 50 %
        :align: center

    Same as the Cascade Transform except it only applies the S12 to the incident b_p-wave.

    Parameters
    ----------
    name : str
        Name of the transform instance.
    ports : list
        List of ports where the transform is applied.
    filename : str
        Filename of (.s2p) S-parameter file that contains the transform coefficients.

    """

    _num_ports = 1
    _domain = Domain.FF
    _device = "cpu"
    _preview_filename = ":/PNG/cascade_circuit_transform.png"
    _default_filename = "Thru.s2p"
    _color_code = Settings().color_map[transform_color_map[Domain.FF]]
    display_order = ["name", "ports", "instrument_flags", "file", "transpose", "swap_ports"]
    optimize = False

    def __new__(cls, name: str = "Receiver Gain", ports: tuple = (2,), instrument_flags=Instrument.RFRECEIVER,
                abcd: th.Tensor = th.eye(2), filename: str = "", transpose: bool = False, swap_ports: bool = False):
        self = super(ReceiverGainTransform, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                                         abcd=abcd, filename=filename, transpose=transpose, swap_ports=swap_ports)
        return self

    def __init__(self, name: str = "Receiver Gain", ports: tuple = (2,), instrument_flags=Instrument.RFRECEIVER,
                 abcd: th.Tensor = th.eye(2), filename: str = "", transpose: bool = False, swap_ports: bool = False):
        super(ReceiverGainTransform, self).__init__(name, ports, instrument_flags=instrument_flags,
                                                    abcd=abcd, filename=filename, transpose=transpose, swap_ports=swap_ports)
        self.__info__()

    def __info__(self):
        super(ReceiverGainTransform, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###

    def _set_data(self, data):
        data = data.repeat(Settings().f_points * Settings().t_points, 1, 1) if data.dim() == 2 else data
        data = self._format_data(data)
        self._data_ = 1/a2s(data)[:, 0:1, 1:2]

    def get_stimulus(self, *args):
        return args

    def set_stimulus(self, *args):
        return args

    def get_response(self, *args):
        p = self.ports[0]
        v, i = args[p][0], args[p][1]
        s = v.shape

        shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
        v, i = v.reshape(shape), i.reshape(shape)
        b = viz2baz(v, i)[0]
        a = viz2baz(v, i)[1]
        b *= self._data_
        v = baz2viz(b, a)[0]
        i = baz2viz(b, a)[1]

        res = list(args)
        res[p][0], res[p][1] = v.reshape(s), i.reshape(s)
        return tuple(res)


class SourceReconstructionFilter(FFTransform):
    r"""Reconstruct the frequency response of a sampled signal.

        Parameters
        ----------
        name : str
            Name of the transform instance.
        ports : list
            List of ports where the transform is applied.
        mode : enum
            LineCode, default is LineCode.NRZ
        """

    _num_ports = 1
    _domain = Domain.FF
    _device = "cpu"
    _preview_filename = ":/PNG/cascade_circuit_transform.png"
    _default_filename = "Thru.s2p"
    _color_code = Settings().color_map[transform_color_map[Domain.FF]]
    display_order = ["name", "ports", "instrument_flags", "mode"]
    optimize = False

    def __new__(cls, name: str = "Reconstruction Filter", ports: tuple = (1,), instrument_flags=Instrument.LFSOURCE,
                mode=LineCode.NRZ):
        self = super(SourceReconstructionFilter, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                                              data=mode)
        self._mode = mode
        return self

    def __getnewargs__(self):
        return tuple(list(super(SourceReconstructionFilter, self).__getnewargs__()) +
                     [self._mode])

    def __init__(self, name: str = "Reconstruction Filter", ports: tuple = (1,), instrument_flags=Instrument.LFSOURCE,
                 mode=LineCode.NRZ):
        super(SourceReconstructionFilter, self).__init__(name, ports, instrument_flags=instrument_flags,
                                                         data=mode)
        self.__info__()
        self._set_data(self._mode)

    def __setstate__(self, state):
        self.__info__()
        self._set_data(self._mode)

    def __info__(self):
        super(SourceReconstructionFilter, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["file"] = Info("file", read=False, write=False, check=False)
        self.info["mode"] = Info("mode", read=True, write=True, check=False)

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode
        self._set_data(self._mode)

    @property
    def instrument_flags(self):
        return self._instrument_flags

    @instrument_flags.setter
    def instrument_flags(self, instrument_flags):
        self._instrument_flags = instrument_flags
        self._set_data(self._mode)

    def _format_data(self, data):
        data = data.reshape((Settings().t_points, Settings().f_points, 1, 1))
        if not self.instrument_flags & Instrument.LF:
            data[:, 0, 0, 0] = 1.0
        if not self.instrument_flags & Instrument.RF:
            data[:, 1:, 0, 0] = 1.0
        data = data.reshape((-1, 1, 1))
        return data

    def _set_data(self, mode):
        fs = 1/Settings().t_step
        f = th.linspace(-fs/2, fs/2, Settings().t_points, dtype=si_dtype_map[SI.A])
        if mode == LineCode.NRZ:
            x = mt.pi*f/fs
            x[x.abs() < 1e-100] = 1e-100
            data = th.sin(x)/x
        elif mode == LineCode.RZ:
            x = mt.pi*f/2*fs
            x[th.abs(x) < 1e-100] = 1e-100
            data = 1/2*th.sin(x)/x
        elif mode == LineCode.DOUBLET:
            x = mt.pi*f/2*fs
            x[x.abs() < 1e-100] = 1e-100
            data = 1/2*th.sin(x)/x*th.exp(1j*x) - 1/2*th.sin(x)/x*th.exp(-1j*x)
        else:
            raise ValueError("Unknown Reconstruction Filter")
        data = data.reshape(Settings().t_points, 1, 1)
        data = data.repeat(Settings().f_points, 1, 1)
        data = self._format_data(data)
        self._data_ = data

    def get_stimulus(self, *args):
        p = self.ports[0]
        v = args[p][0]
        s = v.shape

        shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
        v = v.reshape(shape)
        v *= self._data_

        res = list(args)
        res[p][0] = v.reshape(s)
        return res

    def set_stimulus(self, *args):
        p = self.ports[0]
        v, i = args[p][0], args[p][1]
        s = v.shape

        shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
        v = v.reshape(shape)
        v /= self._data_

        res = list(args)
        res[p][0] = v.reshape(s)
        return res

    def get_response(self, *args):
        return args


class ReceiverReconstructionFilter(FFTransform):
    r"""Reconstruct the frequency response of a sampled signal.

        Parameters
        ----------
        name : str
            Name of the transform instance.
        ports : list
            List of ports where the transform is applied.
        mode : enum
            LineCode, default is LineCode.NRZ
        """

    _num_ports = 1
    _domain = Domain.FF
    _device = "cpu"
    _preview_filename = ":/PNG/cascade_circuit_transform.png"
    _default_filename = "Thru.s2p"
    _color_code = Settings().color_map[transform_color_map[Domain.FF]]
    display_order = ["name", "ports", "instrument_flags", "mode"]
    optimize = False

    def __new__(cls, name: str = "Reconstruction Filter", ports: tuple = (1,), instrument_flags=Instrument.LFRECEIVER,
                mode=LineCode.NRZ):
        self = super(ReceiverReconstructionFilter, cls).__new__(cls, name, ports, instrument_flags=instrument_flags,
                                                                data=mode)
        self._mode = mode
        return self

    def __getnewargs__(self):
        return tuple(list(super(ReceiverReconstructionFilter, self).__getnewargs__()) +
                     [self._mode])

    def __init__(self, name: str = "Reconstruction Filter", ports: tuple = (1,), instrument_flags=Instrument.LFRECEIVER,
                 mode=LineCode.NRZ):
        super(ReceiverReconstructionFilter, self).__init__(name, ports, instrument_flags=instrument_flags,
                                                           data=mode)
        self.__info__()
        self._set_data(self._mode)

    def __setstate__(self, state):
        self.__info__()
        self._set_data(self._mode)

    def __info__(self):
        super(ReceiverReconstructionFilter, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["file"] = Info("file", read=False, write=False, check=False)
        self.info["mode"] = Info("mode", read=True, write=True, check=False)

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode
        self._set_data(self._mode)

    @property
    def instrument_flags(self):
        return self._instrument_flags

    @instrument_flags.setter
    def instrument_flags(self, instrument_flags):
        self._instrument_flags = instrument_flags
        self._set_data(self._mode)

    def _format_data(self, data):
        data = data.reshape(Settings().t_points, Settings().f_points, 1, 1)
        if not self.instrument_flags & Instrument.LF:
            data[:, 0, 0, 0] = 1.0
        if not self.instrument_flags & Instrument.RF:
            data[:, 1:, 0, 0] = 1.0
        data = data.reshape(-1, 1, 1)
        return data

    def _set_data(self, mode):
        fs = 1/Settings().t_step
        f = th.linspace(-fs/2, fs/2, Settings().t_points, dtype=si_dtype_map[SI.B])
        if mode == LineCode.NRZ:
            x = mt.pi*f/fs
            x[x.abs() < 1e-100] = 1e-100
            data = th.sin(x)/x
        elif mode == LineCode.RZ:
            x = mt.pi*f/2*fs
            x[x.abs() < 1e-100] = 1e-100
            data = 1/2*th.sin(x)/x
        elif mode == LineCode.DOUBLET:
            x = mt.pi*f/2*fs
            x[x.abs() < 1e-100] = 1e-100
            data = 1/2*th.sin(x)/x*th.exp(1j*x) - 1/2*th.sin(x)/x*th.exp(-1j*x)
        else:
            raise ValueError("Unknown Reconstruction Filter")
        data = data.reshape(Settings().t_points, 1, 1)
        data = data.repeat(Settings().f_points, 1, 1)
        data = self._format_data(data)
        self._data_ = data

    def get_stimulus(self, *args):
        return args

    def set_stimulus(self, *args):
        return args

    def get_response(self, *args):
        p = self.ports[0]
        v, i = args[p][0], args[p][1]
        s = v.shape

        shape = (-1, Settings().t_points*Settings().f_points, 1, 1)
        v = v.reshape(shape)
        v /= self._data_

        res = list(args)
        res[p][0] = v.reshape(s)
        return res
