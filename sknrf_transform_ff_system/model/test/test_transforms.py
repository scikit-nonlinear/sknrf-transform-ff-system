import os
import unittest
import pickle

import torch as th
from  numpy.testing import *

from sknrf.enums.device import Instrument
from sknrf.enums.runtime import SI, si_dtype_map
from sknrf.settings import Settings
from sknrf.model.base import AbstractModel
from sknrf_transform_ff_system.model.system import CascadeTransform, SourceIQGainTransform, SourceGainTransform, ReceiverGainTransform
from sknrf_transform_ff_system.model.system import SourceReconstructionFilter, ReceiverReconstructionFilter, LineCode


__author__ = 'dtbespal'


class TestTransform(unittest.TestCase):
    class_ = CascadeTransform
    args = ("Cascade", (0,), Instrument.RF, th.rand((Settings().t_points*Settings().f_points, 2, 2), dtype=si_dtype_map[SI.G]), "",  False, False)
    filename = os.sep.join([Settings().data_root, "saved_transforms", "Thru.s2p"])

    @classmethod
    def setUpClass(cls):
        AbstractModel.init_test(cls.__name__)
        cls.p_filename = os.sep.join([Settings().data_root, "testdata", "saved_state.p"])

    def setUp(self):
        self.transform = self.class_(*self.args)

    def test_default_args(self):
        self.transform = self.class_()
        self.test_get_stimulus()
        self.test_set_stimulus()
        self.test_get_response()

    def test_get_stimulus(self):
        shape = (1, Settings().t_points, Settings().f_points)
        args = []
        class_ = th.Tensor
        for port_index in range(Settings().num_ports + 1):
            port_args = []
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.V]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.I]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.Z]))
            args.append(port_args)
        res_args = self.transform.get_stimulus(*args)
        for port_index in range(Settings().num_ports + 1):
            self.assertIsInstance(res_args[port_index][0], class_)
            self.assertIsInstance(res_args[port_index][1], class_)
            self.assertIsInstance(res_args[port_index][2], class_)

    def test_set_stimulus(self):
        shape = (1, Settings().t_points, Settings().f_points)
        args = []
        class_ = th.Tensor
        for port_index in range(Settings().num_ports + 1):
            port_args = []
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.V]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.I]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.Z]))
            args.append(port_args)
        res_args = self.transform.set_stimulus(*args)
        for port_index in range(Settings().num_ports + 1):
            self.assertIsInstance(res_args[port_index][0], class_)
            self.assertIsInstance(res_args[port_index][1], class_)
            self.assertIsInstance(res_args[port_index][2], class_)

    def test_get_response(self):
        shape = (1, Settings().t_points, Settings().f_points)
        args = []
        class_ = th.Tensor
        for port_index in range(Settings().num_ports + 1):
            port_args = []
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.V]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.I]))
            port_args.append(th.rand(shape, dtype=si_dtype_map[SI.Z]))
            args.append(port_args)
        res_args = self.transform.get_response(*args)
        for port_index in range(Settings().num_ports + 1):
            self.assertIsInstance(res_args[port_index][0], class_)
            self.assertIsInstance(res_args[port_index][1], class_)
            self.assertIsInstance(res_args[port_index][2], class_)

    def test_save(self):
        if os.path.exists(self.p_filename):
            os.remove(self.p_filename)
        pickle.dump(self.transform, open(self.p_filename, "wb"))
        self.assertTrue(os.path.exists(self.p_filename))

    def test_save_load(self):
        self.test_save()
        del self.transform

        self.transform = pickle.load(open(self.p_filename, "rb"))
        self.assertIsInstance(self.transform, self.class_)

    def test_file(self):
        if len(self.filename):
            file = open(self.filename, 'r')
            file.close()
            self.transform.file = file
            self.assertTrue(self.transform.file.closed)

    def test_color(self):
        self.assertIsInstance(self.transform.color(), str)

    def test_release(self):
        self.transform.release()

    def test_ports(self):
        ports = tuple(range(self.transform._num_ports+1))
        self.assertRaisesRegex(ValueError,
                               "%s is a %d-Port Transform" % (self.transform.__class__.__name__, self.transform._num_ports,),
                               self.transform.__class__.ports.fset,
                               self.transform, ports)
        ports = list(range(self.transform._num_ports))
        ports[0] = Settings().num_ports + 1
        ports = tuple(ports)
        self.assertRaisesRegex(ValueError,
                               "Ports entry exceeds the maximum number of ports: %d " % Settings().num_ports,
                               self.transform.__class__.ports.fset,
                               self.transform, ports)
        ports = tuple(range(self.transform._num_ports))
        self.transform.ports = ports

    def tearDown(self):
        pass


class TestCascadeTransform(TestTransform):
    class_ = CascadeTransform
    args = ("Cascade", (0,), Instrument.RF, th.rand((Settings().t_points*Settings().f_points, 2, 2), dtype=si_dtype_map[SI.G]), "",  False, False)
    filename = os.sep.join([Settings().data_root, "saved_transforms", "Thru.s2p"])

    def test_transpose(self):
        abcd = self.transform._abcd.clone()
        self.transform.transpose = True
        self.assertTrue(self.transform.transpose)
        self.transform.transpose = False
        self.assertFalse(self.transform.transpose)
        assert_allclose(self.transform._abcd, abcd)

    def test_swap_ports(self):
        abcd = self.transform._abcd.clone()
        self.transform.swap_ports = True
        self.assertTrue(self.transform.swap_ports)
        self.transform.swap_ports = False
        self.assertFalse(self.transform.swap_ports)
        assert_allclose(self.transform._abcd, abcd)

    def test_insrument_flags(self):
        self.transform.instrument_flags = Instrument.LF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.RF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.ALL
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))


class TestSourceIQTransform(TestTransform):
    class_ = SourceIQGainTransform
    args = ("Source IQ Gain", (1,), Instrument.RFSOURCE, th.rand((1,), dtype=si_dtype_map[SI.A]))
    filename = ""

    def test_gain(self):
        self.transform.gain = th.ones((1,), dtype=si_dtype_map[SI.A])
        self.assertTrue(self.transform.gain)
        assert_allclose(self.transform.gain, 1.0)

    def test_insrument_flags(self):
        self.transform.instrument_flags = Instrument.LF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.RF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.ALL
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))


class TestSourceGainTransform(TestTransform):
    class_ = SourceGainTransform
    args = ("Source Gain", (1,), Instrument.RFSOURCE, th.rand((Settings().t_points*Settings().f_points, 2, 2), dtype=si_dtype_map[SI.A]), "", False, False)
    filename = ""

    def test_insrument_flags(self):
        self.transform.instrument_flags = Instrument.LF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.RF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.ALL
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))


class TestReceiverGainTransform(TestTransform):
    class_ = ReceiverGainTransform
    args = ("Receiver Gain", (1,), Instrument.RFRECEIVER, th.rand((Settings().t_points*Settings().f_points, 2, 2), dtype=si_dtype_map[SI.B]), "", False, False)
    filename = ""

    def test_insrument_flags(self):
        self.transform.instrument_flags = Instrument.LF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.RF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.ALL
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))


class TestSourceReconstructionFilter(TestTransform):
    class_ = SourceReconstructionFilter
    args = "Reconstruction Filter", (1,), Instrument.LFSOURCE, LineCode.NRZ
    filename = ""

    def test_mode(self):
        self.transform.mode = LineCode.NRZ
        self.assertTrue(self.transform.mode == LineCode.NRZ)
        self.transform.mode = LineCode.RZ
        self.assertTrue(self.transform.mode == LineCode.RZ)
        # self.transform.mode = LineCode.DOUBLET
        # self.assertTrue(self.transform.mode == LineCode.DOUBLET)

    def test_insrument_flags(self):
        self.transform.instrument_flags = Instrument.LF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.RF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.ALL
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))


class TestReceiverReconstructionFilter(TestTransform):
    class_ = ReceiverReconstructionFilter
    args = "Reconstruction Filter", (1,), Instrument.RFRECEIVER, LineCode.NRZ
    filename = ""

    def test_mode(self):
        self.transform.mode = LineCode.NRZ
        self.assertTrue(self.transform.mode == LineCode.NRZ)
        self.transform.mode = LineCode.RZ
        self.assertTrue(self.transform.mode == LineCode.RZ)
        # self.transform.mode = LineCode.DOUBLET
        # self.assertTrue(self.transform.mode == LineCode.DOUBLET)

    def test_insrument_flags(self):
        self.transform.instrument_flags = Instrument.LF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.RF
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
        self.transform.instrument_flags = Instrument.ALL
        assert_equal(self.transform._abcd.shape, (Settings().t_points*Settings().f_points, 2, 2))
